﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data;

namespace ThreeTierLookup
{
    public class TaxCodeBL
    {
        public static string connstr;

        private TaxCodeDL taxcodedl = new TaxCodeDL(connstr);

        public TaxCodeBL(string connectionstr)
        {
            connstr = connectionstr;
        }

        public DataTable getTaxCode(string filter, string filtervalue)
        {
            filtervalue = "%" + filtervalue + "%";
            return taxcodedl.getTaxCode(filter, filtervalue);
        }
    }
}