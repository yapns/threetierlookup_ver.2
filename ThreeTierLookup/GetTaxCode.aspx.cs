﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

namespace ThreeTierLookup
{
    public partial class GetTaxCode : System.Web.UI.Page
    {
        TaxCodeBL taxcodebl = new TaxCodeBL(ConfigurationManager.ConnectionStrings["LookUpConnection"].ToString());
       
         protected void Page_Load(object sender, EventArgs e)
         {
             if (!Page.IsPostBack)
              loadddl();
         }

         private void loadddl()
         {
             ddltaxcodeorpercentage.DataSource = new DataTable();
             ddltaxcodeorpercentage.DataBind();

             ListItem searchTaxcode = new ListItem("Tax Code", "tax_code");
             ListItem searchPercentage = new ListItem("Percentage of GST on purchases", "percentage_");
             
             ddltaxcodeorpercentage.Items.Insert(0, searchTaxcode);
             ddltaxcodeorpercentage.Items.Insert(1, searchPercentage);

             gvTaxCode.DataSource = new DataTable();
             gvTaxCode.DataBind();
         }
  
         protected void btnsearch_Click1(object sender, EventArgs e)
         {  gvTaxCode.DataSource = taxcodebl.getTaxCode(ddltaxcodeorpercentage.SelectedValue.ToString(), txtboxvalue.Text);
             gvTaxCode.DataBind();
         }

         protected void gvTaxCode_RowDataBound(object sender, GridViewRowEventArgs e)
         {
             if (e.Row.RowType == DataControlRowType.DataRow)
             {
                 Label lbltaxcode = (Label)e.Row.FindControl("lblTaxcode");
                 string taxcode = lbltaxcode.Text;

                 Label lblpercentage = (Label)e.Row.FindControl("lblPercentage");
                 string percentage = lblpercentage.Text;

                 e.Row.Attributes.Add("onclick", string.Format("callParent('{0}','{1}') ", taxcode, percentage));
             }
         }
       
    }
}