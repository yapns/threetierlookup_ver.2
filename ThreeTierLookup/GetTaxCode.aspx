﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GetTaxCode.aspx.cs" Inherits="ThreeTierLookup.GetTaxCode" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <base target='_self' />
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
     </style>
     
     <script type="text/javascript">
         function callParent(lbltaxcode, lblpercentage)
        {
            var rtnValue = {};
            rtnValue.taxcode = lbltaxcode;
            rtnValue.percentage = lblpercentage;
            window.returnValue = rtnValue;
            window.close();
       }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;
    
        Tax Code/ Percentage: <asp:DropDownList ID="ddltaxcodeorpercentage" 
            runat="server" Width="220px">
        </asp:DropDownList>
        <br />
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Value:&nbsp;
        <asp:TextBox ID="txtboxvalue" runat="server"></asp:TextBox>
        <br />
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnsearch" runat="server" Text="search" 
            onclick="btnsearch_Click1" />
    
        <br />
        <br />
        <table class="style1">
            <tr>
                <td>
                    &nbsp;</td>
                <td>
        <asp:GridView ID="gvTaxCode" runat="server" AutoGenerateColumns="False" 
                        onrowdatabound="gvTaxCode_RowDataBound" style="margin-left: 127px">
            <Columns>
                <asp:TemplateField HeaderText="Tax Code">
                        <ItemTemplate>
                            <asp:Label ID="lblTaxcode" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "Taxcode") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Percentage">
                        <ItemTemplate>
                            <asp:Label ID="lblPercentage" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "Percentage") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
            </Columns>
        </asp:GridView>
    
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    
        <br />
    
    </div>
    </form>
</body>
</html>
