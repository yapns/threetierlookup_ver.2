﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace ThreeTierLookup
{
    public class TaxCodeDL
    {
        private string connstr;

        public TaxCodeDL(string connstr)
        { this.connstr = connstr;
        }

        public DataTable getTaxCode(string filter, string filtervalue)
        {
            DataTable dt = new DataTable();

            using (SqlConnection conn = new SqlConnection(connstr))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conn;


                    if (filter == "tax_code")
                    {
                        cmd.CommandText = ("SELECT * FROM Tax_Code_Names WHERE tax_code LIKE @value");
                    }
                    else if (filter == "percentage_")
                    {
                        cmd.CommandText = ("SELECT * FROM Tax_Code_Names WHERE percentage_ LIKE @value +'%' ");
                    }


                    cmd.CommandType = CommandType.Text;
                    conn.Open();

                    cmd.Parameters.AddWithValue("@value", filtervalue);
                    cmd.ExecuteNonQuery();
                    SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                    adpt.Fill(dt);

                    dt.Columns["tax_code"].ColumnName = "Taxcode";
                    dt.Columns["percentage_"].ColumnName = "Percentage";
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
            }
            return dt;
        }

    }
}