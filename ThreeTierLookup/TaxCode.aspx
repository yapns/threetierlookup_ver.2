﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TaxCode.aspx.cs" Inherits="ThreeTierLookup.TaxCode" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1 {
            width: 93%;
        }
        .style2
        {
            width: 101px;
        }
        .style4
        {
            width: 153px;
        }
        .style5
        {
            width: 84px;
        }
        .style6
        {
            width: 272px;
        }
        .style7
        {
            width: 30px;
            height: 30px;
        }
    </style>

        <script type="text/javascript">
            function openwindow() 
            {
                var retval = window.showModalDialog('GetTaxCode.aspx');
                populateTextbox(retval.taxcode, retval.percentage);
            }

            function populateTextbox(taxcode, percentage) 
            {
                document.getElementById("<%=txtTaxCode.ClientID%>").value = taxcode;
                document.getElementById("<%=txtPercentage.ClientID%>").value = percentage;
            }
        
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <h2><u>Tax Code Look Up Form</u></h2>

        <table class="style1">
            <tr>
                <td class="style2">
                    Tax Code:</td>
                <td class="style4">
                    <asp:TextBox ID="txtTaxCode" runat="server"></asp:TextBox>
                </td>
                <td class="style5">
                    Percentage:</td>
                <td class="style6">
                    <asp:TextBox ID="txtPercentage" width="220px" runat="server"></asp:TextBox>
                </td>
                <td>
                    <img class="style7" src="search.jpg" onclick="javascript: openwindow()"/></td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;</td>
                <td class="style4">
                    &nbsp;</td>
                <td class="style5">
                    &nbsp;</td>
                <td class="style6">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
